var group__draggers =
[
    [ "SoAngle1Dragger", "classSoAngle1Dragger.html", [
      [ "SoAngle1Dragger", "classSoAngle1Dragger.html#a184d1050e70a7e5930e580ced49c1935", null ],
      [ "~SoAngle1Dragger", "classSoAngle1Dragger.html#afa4c9d0cbd5327f384e71d5487499a98", null ],
      [ "copyContents", "classSoAngle1Dragger.html#ad48a28e891cdfd2b68ed1bc50f0e7f87", null ],
      [ "doneCB", "classSoAngle1Dragger.html#ab3c4d0db52548c1451e71ca22a8d5ee2", null ],
      [ "drag", "classSoAngle1Dragger.html#acc1d23ec92ca5cce49529ebdd1bf28e6", null ],
      [ "dragFinish", "classSoAngle1Dragger.html#a8dabaa7360c0f61a6cf12b1f61b771fd", null ],
      [ "dragStart", "classSoAngle1Dragger.html#a6b9bf67340103651f9f6790ce6f38b6b", null ],
      [ "getProjector", "classSoAngle1Dragger.html#a43d6e9d87f7c5d5ba04ce9a65bdce7cd", null ],
      [ "initClass", "classSoAngle1Dragger.html#ab2e90b63291b0b2b4d7e561981e5e6cd", null ],
      [ "motionCB", "classSoAngle1Dragger.html#a67c5151f1718409aa229465f85a72a1f", null ],
      [ "setProjector", "classSoAngle1Dragger.html#aae055e8a3c5f634cc7cebcf7da1da12b", null ],
      [ "setUpConnections", "classSoAngle1Dragger.html#aa997625a83242694ab07edbfe0b3ed84", null ],
      [ "startCB", "classSoAngle1Dragger.html#a2a6ea68143e9d625c3f628323e5d2675", null ],
      [ "angle", "classSoAngle1Dragger.html#acb4223b44b042b269eb8c636ad277b68", null ]
    ] ],
    [ "SoAngle1Manip", "classSoAngle1Manip.html", [
      [ "SoAngle1Manip", "classSoAngle1Manip.html#aba13d48f4bda9d30877ceae6c3797b33", null ],
      [ "~SoAngle1Manip", "classSoAngle1Manip.html#a77e8bf22dac8e5aa5c6fbb5265ee9696", null ],
      [ "initClass", "classSoAngle1Manip.html#a1bcac7de6683cdd7d67ab74cf4f89541", null ]
    ] ]
];